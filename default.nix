with import <nixpkgs> {};
stdenv.mkDerivation rec {
  name = "ipxe-menu";
  rev = "0b3000bbece3f96d1c4e00aaf93968f6905105bb";

  makeFlags = [ "bin/undionly.kpxe" "EMBED=${menuscript}" ];

  src = fetchgit {
	inherit rev;
    url = "https://git.ipxe.org/ipxe.git";
	sha256 = "15jld9fkcxc8jvqpcv9yx3dkzw7g42867xb0dcwc9prdmkziz464";
  };
  
  # clone gives us a directory with the first seven digits of the rev at the end of it
  # sourceRoot = "ipxe-0b3000b/src";
  setSourceRoot = "sourceRoot=$(ls|grep ipxe-)/src";

	prePatch = ''
		substituteInPlace config/general.h --replace "#undef	DOWNLOAD_PROTO_HTTPS" "#define	DOWNLOAD_PROTO_HTTPS"
	'';

  menuscript = ./intogit.ipxe;

  installPhase = ''
    mkdir -p $out/
    cp bin/undionly.kpxe $out/
  '';

  buildInputs = [
    binutils gcc gnumake perl lzma
  ];
}
